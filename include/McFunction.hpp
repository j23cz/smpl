#ifndef MC_FUNCTIONS_H
#define MC_FUNCTIONS_H

#include "SMPLComand.hpp"
#include "OutsideMcFunction.hpp"
#include <string>
#include <vector>


class McFunction {

public:	
	McFunction();
	McFunction(const McFunction &mcfunction);
	void operator=(const McFunction &mcfunction);
	std::string getName();
		
	void setOutsideFunctions(std::vector<OutsideMcFunction> outside_mc_functions);
	void rename(std::string name);
	void newContent();
	void addContent(std::string content);
	void clean();
	void renameAndNewContent(std::string name);
	void setVarsLists(std::vector <std::string> IntList, std::vector <std::string> TabIntList);
	McFunction copyAndRename(std::string name);

	void parseSMPLCommand(SMPLComand com);
	void saveFunction(std::string dir);

	void setDebug(bool debug);

protected:
	std::string _name = "";
	std::string _content = "";
    std::string _end_content = "";

	std::vector <std::string> _new_int_list;
    std::vector <std::string> _int_list;
	std::vector <std::string> _new_tab_int_list;
	std::vector <std::string> _tab_int_list;
	bool _base_int_tabs = false;
	bool _int_array_index = false;
	bool _int_array_pointer = false;
	int _max_array_size = 0;

	bool _debug = false;
	
	std::vector<OutsideMcFunction> _outside_mc_functions;
private:
	std::string mcPrint(SMPLComand com);
	std::string mcRun(std::string path);
	std::string mcIf(SMPLComand com);
	std::string mcIfWithElse();
	std::string mcElse(SMPLComand com);
	std::string mcElseWithIf(SMPLComand com);
	std::string mcSubcomand(SMPLComand com);


    //operation on int (IOF)
	//int
	std::string mcAddInt(std::string a);
	std::string mcAddIntTab(std::string a);

	std::string mcOperationInt(SMPLComand com);
	//=
	std::string mcIntSet(std::string a, std::string b);
	
	//+ -
	std::string mcIntAddEqual(std::string result, std::string a);
	std::string mcIntSubtractEqual(std::string result, std::string a);
	//* =
	std::string mcIntMultiplyEqual(std::string result, std::string a);
	std::string mcIntDivideEqual(std::string result, std::string a);
	//%
	std::string mcIntModuloEqual(std::string result, std::string a);
	//EOOF
	bool isNumber(std::string word);
	bool isCmpOperator(std::string word);
	bool IfArray(std::string a);
};

#endif // MC_FUNCTIONS_H