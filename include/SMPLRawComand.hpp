#ifndef SMPL_RAW_COMAND
#define SMPL_RAW_COMAND

#include <string>
#include <vector>

class SMPLRawComand {
public:
    SMPLRawComand();
    SMPLRawComand(std::string comand, int start, int stop, std::string file_path);
    void operator=(const SMPLRawComand &SMPLRawComand);
    
    void print();
    
public:
    std::string _comand;
    int _start;
    int _stop;
    std::string _file_path;
};

#endif // SMPL_RAW_COMAND
