#include "OutsideMcFunction.hpp"
#include "SMPLComand.hpp"
#include "SMPLRawComand.hpp"
#ifndef SMPL_FILE_READER
#define SMPL_FILE_READER

#include <string>
#include <vector>
#include <map>

class SMPLFileReader {

public:
    SMPLFileReader();
    void setPath(std::string path);
    void setDebug(bool debug);

    std::vector<SMPLComand> readFile();
    
    
    
    void addOutsideMcFunctionToMap(std::vector<OutsideMcFunction> functions);
private:
    void fillMap();

    SMPLComand SMPLRawComandToSMPLComand(SMPLRawComand SMPL_raw_comand);
    std::vector<SMPLComand> ComandsSimplifier( std::vector<SMPLComand> list_comands);
    std::vector<SMPLComand> ComandSimplify(SMPLComand comand, int* tmp_varable);
    
    std::string _path;
    std::vector<std::string> _loaded_files;
    std::map<std::string, bool> _base_comands;
    bool _debug = false;
};

#endif // SMPL_FILE_READER
