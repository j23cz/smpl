#ifndef SMPL_H
#define SMPL_H

#include "SMPLComand.hpp"
#include "McFunction.hpp"
#include "OutsideMcFunction.hpp"
#include <stack>
#include <string>
#include <vector>

#define IDLE -1
#define INIT 0
#define FUNCTION 1

class SMPL {


public:
    SMPL();
	void setName(std::string name);
	void setOutPath(std::string outPath);
	void setDebug(bool debug);
	void setOutsideFunctions(std::vector<OutsideMcFunction> outside_mc_functions);

	void parseSMPLComands(std::vector<SMPLComand> comands);
protected:
	std::string _name = "";
	std::string _out_path = ".";
	bool _debug = false;

    int _state = IDLE;
	int _function_names = 0;
	McFunction _mcfunction;
	std::stack<McFunction> _functions;
	std::vector<OutsideMcFunction> _outside_mc_functions;
};


#endif // SMPL_H