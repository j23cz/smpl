#ifndef SMPL_COMAND
#define SMPL_COMAND

#include <string>
#include <vector>

class SMPLComand {
public:
    
    SMPLComand(std::string name, std::vector<std::string> args, std::string lines, std::string file);
    void print();
    void error(std::string error);
    
public:
    std::string _name;
    std::vector<std::string> _args;
    std::string _lines;
    std::string _file;
};

#endif // SMPL_COMAND
