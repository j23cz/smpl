#ifndef OUTSIDE_MC_FUNCTIONS_H
#define OUTSIDE_MC_FUNCTIONS_H

#include "SMPLComand.hpp"
#include <string>
#include <vector>
#include <tuple>

class Task {
public:
    u_int _id;
    // 0 text           --string
    // 1 arg            --int
    // 2 if asgs >= n   --int
    int _int_value;
    std::string _string_value;
    

    Task();
    Task(int id, int int_value, std::string string_value); 
    std::tuple<std::string, int> doTask(SMPLComand com);
    void print();
    // output , status
    // -1 -- imposible 
    // 0  -- std output
    // 1  -- if_succ
    // 2  -- if_fail
};


class OutsideMcFunction {
public:
    std::string _name;
    u_int _min_args;
    std::vector<int> _args_types;
    // 0 -- any
    // 1 -- int
    // 2 -- string
    // 3 -- entity 
private:
    std::vector<Task> _tasks;

public:
    static OutsideMcFunction loadOutsideMcFunction(std::string path_to_file);
    static std::vector<OutsideMcFunction> loadOutsideMcFunctions(std::string path);
    std::string parseSMPLComand(SMPLComand com);
    void print();

private:
    OutsideMcFunction();
};




#endif //OUTSIDE_MC_FUNCTIONS_H