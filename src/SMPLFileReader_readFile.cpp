#include "include/SMPLFileReader.hpp"
#include "include/SMPLRawComand.hpp"
#include "include/SMPLComand.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

vector<SMPLComand> SMPLFileReader::readFile() {
    char c0 = ' ';
    char c1 = ';';
    int mode = 0;
    // 0 -- std;
    // 1 -- commet line;
    // 2 -- string text;
    // 3 -- mc code line;
    // 4 -- sqer bracket;
    
    bool string_text = false;
    bool mc_text = false;
    int line = 1;
    
    vector<SMPLRawComand> list_of_raw_commands;
    SMPLRawComand raw = SMPLRawComand();
    raw._start = line;
    
    string SMPL_raw_command = string();
    std::ifstream inputFile;
	inputFile.open(_path);
    
    
	if ( !inputFile.is_open() ) {
        throw "Nie poprawny plik";
		inputFile.close();
	}
	while (inputFile.get(c0)) {
        switch (mode) {
            case 0:
                switch (c0)  {                        
                    case '"':
                        if(c1 != '\\') {
                            mode = 2;
                            if(!raw._start) raw._start = line;
                            SMPL_raw_command.append(1, c0);
                        } else {
                            if(!raw._start) raw._start = line;
                            SMPL_raw_command.append(1, c0);
                        }
                        break;
                    case '[':
                        if(c1 != '\\') {
                            mode = 4;
                            if(!raw._start) raw._start = line;
                            SMPL_raw_command.append(1, c0);
                        } else {
                            if(!raw._start) raw._start = line;
                            SMPL_raw_command.append(1, c0);
                        }
                        break;
                    case '$':
                        if(!raw._start) raw._start = line;
                        SMPL_raw_command.append(1, c0);
                        mode = 3;
                        break;
                    case '\n':
                        line++;
                    case '\t':
                    case ' ':
                        switch (c1)  {
                            case ';':
                                c0 = ';';
                                break;
                            case '\n':
                            case '\t':
                            case ' ':
                                break;
                            default:
                                SMPL_raw_command.append(1, ' ');
                        }
                        break;
                    case ',':
                        SMPL_raw_command.append(1, ' ');
                        SMPL_raw_command.append(1, c0);
                        SMPL_raw_command.append(1, ' ');
                        c0 = ' ';
                        break;
                    case '/':
                        if(c1 == '/') {
                            SMPL_raw_command.erase(SMPL_raw_command.length()-1, 1);
                            mode = 1;
                            break;
                        }
                    case '*':
                    case '+':
                    case '-':
                    case '>':
                    case '<':
                        switch (c1)  {
                            case ' ':
                            case '=':
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                                break;
                            default:
                                SMPL_raw_command.append(1, ' ');
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                        }
                        break;
                    case '=':
                        switch (c1)  {
                            case ' ':
                            case '*':
                            case '/':
                            case '+':
                            case '-':
                            case '>':
                            case '<':
                            case '=':
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                                break;
                            default:
                                SMPL_raw_command.append(1, ' ');
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                        }
                        break;
                    case '(':
                        switch (c1)  {
                            case ' ':
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                                break;
                            default:
                                SMPL_raw_command.append(1, ' ');
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                        }
                        break;
                    case '{':
                    case '}':
                    case ';':
                        if(!raw._start) raw._start = line;
                        SMPL_raw_command.append(1, c0);
                        raw._stop = line;
                        raw._comand = SMPL_raw_command;
                        raw._file_path = _path;
                        
                        list_of_raw_commands.push_back(raw);
                        SMPL_raw_command = string();
                        raw = SMPLRawComand();
                        c0 = ';';
                        break;
                    default:
                        switch (c1)  {
                            case '*':
                            case '/':
                            case '+':
                            case '-':
                            case '>':
                            case '<':
                            case '=':
                            case ')':
                                SMPL_raw_command.append(1, ' ');
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                                break;
                            default:
                                if(!raw._start) raw._start = line;
                                SMPL_raw_command.append(1, c0);
                        }
                }
                break;
            case 1:
                switch (c0)  {
                    case '\n':
                        line++;
                        mode = 0;
                        break;                        
                }
                break;
            case 2:
                SMPL_raw_command.append(1, c0);
                switch (c0)  {
                    case '"':
                        if(c1 != '\\') mode = 0;
                        break;
                }
                break;
            case 3:
                switch (c0)  {
                    case '\n':
                        if(!raw._start) raw._start = line;
                        raw._stop = line;
                        raw._comand = SMPL_raw_command;
                        raw._file_path = _path;
                        
                        list_of_raw_commands.push_back(raw);
                        SMPL_raw_command = string();
                        raw = SMPLRawComand();
                        c0 = ';';
                        line++;
                        mode = 0;
                        
                        break;
                    default:
                        if(!raw._start) raw._start = line;
                        SMPL_raw_command.append(1, c0);
                }
                break;
            case 4:
                SMPL_raw_command.append(1, c0);
                switch (c0)  {
                    case ']':
                        if(c1 != '\\') mode = 0;
                        break;
                }
                break;
        }
        c1 = c0;
    }
	inputFile.close();
    
    vector<SMPLComand> odp;
    for (auto s : list_of_raw_commands) {
        for(int i = 0; i < s._comand.length();) {
            if(s._comand[0] == ' ') {
                s._comand.erase(0, 1);
            } else {
                break;
            }
        }
        //cout << s._comand << endl;
        odp.push_back(SMPLRawComandToSMPLComand(s));
    }
    return ComandsSimplifier(odp);
}
    

    
    
