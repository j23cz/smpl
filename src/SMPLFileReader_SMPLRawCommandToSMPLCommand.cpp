#include "include/SMPLFileReader.hpp"
#include "include/SMPLRawComand.hpp"
#include "include/SMPLComand.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

SMPLComand SMPLFileReader::SMPLRawComandToSMPLComand(SMPLRawComand SMPL_raw_comand) {
    vector<string> args;
    string name = string();
    string tmp = string();
    bool array_bracket = false;
    bool string_text = false;
    if(SMPL_raw_comand._comand[0] == '$') {
        tmp = SMPL_raw_comand._comand;
        tmp.erase(0, 1);
        args.push_back(tmp);
        return SMPLComand("$", args, "", "");
    }
    char s1 = ' ';
    for (char s : SMPL_raw_comand._comand) {
        if(s == ';') break;
        if(!array_bracket) {
            if(!string_text) {
                switch (s)  {
                    case '"':
                        tmp.append(1, s);
                        string_text = true;
                        break;
                    case '[':
                        tmp.append(1, s);
                        array_bracket = true;
                        break;
                    case ' ':
                        if(!tmp.empty()) {
                            if(name.empty()) {
                                if(_base_comands[tmp]) {
                                    name = tmp;
                                    tmp = string();
                                } else {
                                    args.push_back(tmp);
                                    tmp = string();
                                }
                            }else {
                                args.push_back(tmp);
                                tmp = string();
                            }
                        }
                        break;
                    case '(':
                    case ')':
                    case '{':
                        if(!tmp.empty()) {
                            args.push_back(tmp);
                            tmp = string();
                        }
                        tmp.append(1, s);
                        args.push_back(tmp);
                        tmp = string();
                        break;
                    
                    default:
                        tmp.append(1, s);
                        
                }
            } else {
                if(s1 != '\\' && s == '"') {
                    tmp.append(1, s);
                    string_text = false;
                } else if (s1 == '\\' && s == '"') {
                    tmp.append(1, s1);
                    tmp.append(1, s);
                } else if (s1 == '\\' && s == '\\') {
                    tmp.append(1, s);
                } else if (s == '\\') {

                } else {
                    tmp.append(1, s);
                }
            }
        } else {
            tmp.append(1, s);
            if (s == ']') {
                array_bracket = false;
            }
        }
        s1 = s;
    }
    if(name.empty()) {
        if(!tmp.empty())
            if(_base_comands[tmp]) {
                name = tmp;
                tmp = string();
            } else {
                name = "unknown_operation";
                args.push_back(tmp);
                tmp = string();
            }
    }
    if(!tmp.empty()) args.push_back(tmp);
    if(SMPL_raw_comand._start == SMPL_raw_comand._stop) {
        return SMPLComand(name, args, to_string(SMPL_raw_comand._start), SMPL_raw_comand._file_path);
    }
    
    return SMPLComand(name, args, to_string(SMPL_raw_comand._start) + "-" + to_string(SMPL_raw_comand._stop), SMPL_raw_comand._file_path);
}

    

    
    
