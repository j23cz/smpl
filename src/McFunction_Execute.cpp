#include "include/McFunction.hpp"
using namespace std;

string McFunction::mcIf(SMPLComand com) {
	string out = "";
    if (com._args[0] != "(" || com._args[com._args.size()-1-1] != ")") com.error("missing '(' or ')'");
    if(com._name == "while") {
        out += "execute if";  
    } else if (com._name == "if" || com._name == "unless") {
        out += "execute " + com._name;       
    } else if (com._name == "if_with_else" || com._name == "unless_with_else") {
        if(com._name == "if_with_else") com._name = "if";
        if(com._name == "unless_with_else") com._name = "unless";
        out += "execute store success score .$IF_SUCC SMPL " + com._name;       
    } else throw  "file: " + com._file + " in line: " + com._lines + " unknown operation" + com._name;
    if (com._args[1][0] == '@') { // entity
        if (com._args[0] != "(" || com._args[2] != ")" || com._args.size() != 3+1) com.error("epected 4 arg");
        out += " entity " + com._args[1];
    } else if (com._args.size() == 10) {
        // ( ~ , ~ , ~ , id ) {
        if (com._args[0] != "(" || com._args[8] != ")" || com._args.size() != 9+1) com.error("epected 11 arg");
        out += " block " + com._args[1] + " " + com._args[3] + " " + com._args[5]  + " " + com._args[7];
    } else {
        if (com._args[0] != "(" || com._args[4] != ")" || com._args.size() != 5+1) com.error("epected 5 arg");
        string a = com._args[1];
        string op = com._args[2];
        string b = com._args[3];
        if (isNumber(a)) {
            out = "scoreboard players set #$if_a_tmp SMPL " + a + "\n" + out;
            a = "#$if_a_tmp";
        }
        if (isNumber(b)) {
            out = "scoreboard players set #$if_b_tmp SMPL " + b + "\n" + out;
            b = "#$if_b_tmp";
        }
        if (!isCmpOperator(op)) com.error("unknown operator");
        if (op == "==") op = "=";
        out += " score " + a + " SMPL " + op + " " + b + " SMPL";
    }
    out += " run " + mcRun(com._args[com._args.size()-1]);
	return out;
}
string McFunction::mcIfWithElse() {
    return "scoreboard players reset .$IF_SUCC SMPL\n";
}

string McFunction::mcElse(SMPLComand com) {
    if (com._args.size() != 1) com.error("epected 0 arg");
    return "execute unless score .$IF_SUCC SMPL matches 1 run " + mcRun(com._args[com._args.size()-1]);
}

string McFunction::mcElseWithIf(SMPLComand com) {
    com._name = com._args[0];
    com._args.erase(com._args.begin());
    return "execute unless score .$IF_SUCC SMPL matches 1 run " + mcIf(com);
}

string McFunction::mcSubcomand(SMPLComand com) {
    string out = "";
	if (com._args[0] != "(" || com._args[com._args.size()-1-1] != ")") com.error("missing '(' or ')'"); 	
	out += "execute " + com._name + " ";
	for (int i = 1; i<com._args.size()-2; i++) {
        if(i%2 == 1) {
		    out += com._args[i] + " ";
        } else if(com._args[i] != ",") {
            com.error("missing ','");
        }
	}
	out += "run " + mcRun(com._args[com._args.size()-1]);
	return out;
}