#include "include/McFunction.hpp"
#include <ctype.h>
#include <fstream>
#include <iostream>
#include <set>

using namespace std;

McFunction::McFunction() {
	this->_name = "";
	this->_content = "";
    this->_end_content = "";

	this->_base_int_tabs = false;
    this->_int_array_index = false;
	this->_int_array_pointer = false;
	this->_max_array_size = false;

	this->_debug = false;
}
McFunction::McFunction(const McFunction &mcfunction){
	this->_name = mcfunction._name;
	this->_content = mcfunction._content;
    this->_end_content = mcfunction._end_content;

    
	this->_int_list = mcfunction._int_list;
	this->_tab_int_list = mcfunction._tab_int_list;
	
    this->_base_int_tabs = mcfunction._base_int_tabs;
    this->_int_array_index = mcfunction._int_array_index;
	this->_int_array_pointer = mcfunction._int_array_pointer;
	this->_max_array_size = mcfunction._max_array_size;

	this->_debug = mcfunction._debug;
}
void McFunction::operator=(const McFunction &mcfunction) {
	this->_name = mcfunction._name;
	this->_content = mcfunction._content;
    this->_end_content = mcfunction._end_content;

	this->_new_int_list.clear();
	this->_new_tab_int_list.clear();
	this->_int_list = mcfunction._int_list;
	this->_tab_int_list = mcfunction._tab_int_list;

    this->_base_int_tabs = mcfunction._base_int_tabs;
    this->_int_array_index = mcfunction._int_array_index;
	this->_int_array_pointer = mcfunction._int_array_pointer;
	this->_max_array_size = mcfunction._max_array_size;
	
	this->_debug = mcfunction._debug;
}
bool McFunction::isNumber(string word) {
		int dummy_int;
		int scan_value = sscanf( word.c_str(), "%d", &dummy_int);
		return scan_value;
}
bool McFunction::isCmpOperator(string word) {
		return (word=="!=" || word=="==" || word=="<=" || word==">=" || word=="<" || word==">");
}
bool McFunction::IfArray(string a) {
	if(a.find('[') != string::npos && a.find(']') != string::npos) return 1;
	return 0;
}
void McFunction::setOutsideFunctions(vector<OutsideMcFunction> outside_mc_functions) {
	this->_outside_mc_functions = outside_mc_functions;
}

void McFunction::setDebug(bool debug) {
	_debug = debug;
}

void McFunction::newContent() {
	this->_content = "";
	this->_end_content = "";
}
void McFunction::addContent(string content) {
	this->_content += content;
}
void McFunction::rename(string name) {
	this->_name = name;
}
void McFunction::renameAndNewContent(string name) {
	this->_content = "";
	this->_end_content = "";
	this->_name = name;
}
McFunction McFunction::copyAndRename(string name) {
    McFunction mc_function = McFunction(*this);
    mc_function.rename(name);
	return mc_function;
}
void McFunction::setVarsLists(vector <string> int_list, vector <string> tab_int_list) {
    this->_new_int_list.clear();
	this->_new_tab_int_list.clear();
	this->_int_list = int_list;
	this->_tab_int_list = tab_int_list;
}
void McFunction::saveFunction(string dir) {
	ofstream file;
	file.open(dir + _name + ".mcfunction", ios::out);
	file << _content;
    file << _end_content;
	file.close();
	if(_debug) {
	cout << "function path: " + dir + _name + ".mcfunction" << endl;
	cout << endl;
	cout << _content << endl;
	cout << _end_content << endl;
	cout << "----------------------------------------------" << endl;
	}
}
//--------------------------------------//
void McFunction::parseSMPLCommand(SMPLComand com) {
	if (com._name == "$") {
        for (auto arg : com._args) _content += arg;
		_content += "\n";
	} else if (com._name == "print") {
		_content += mcPrint(com);
	} else if (com._name == "if" || com._name == "unless") {
		_content += mcIf(com);
	} else if (com._name == "if_with_else" || com._name == "unless_with_else") {
		_content += mcIf(com);
		_end_content += mcIfWithElse();
	} else if (com._name == "else") {
		if(com._args[0] == "if" || com._args[0] == "unless" ) {
			_content += mcElseWithIf(com);
		} else if(com._args[0] == "if_with_else" || com._args[0] == "unless_with_else" ) {
			_content += mcElseWithIf(com);
			//_end_content += mcIfWithElse();
		} else {
			_content += mcElse(com);
		}
	} else if (com._name == "while") {
		_end_content += mcIf(com);
	} else if(com._name == "as" || com._name == "at" || com._name == "positioned") {
		_content += mcSubcomand(com);
	}else if(com._name == "int") {
        if(com._args.size() != 1) com.error("expected one argument");
		_content += mcAddInt(com._args[0]);
	}else if(
		com._name == "=" ||
		com._name == "+=" ||
		com._name == "-=" ||
		com._name == "*=" ||
		com._name == "/=" ||
		com._name == "%="
	) {
		_content += mcOperationInt(com);
	}else if(com._name == "run") {
        if(com._args.size() != 3) com.error("expected one argument");
		_content += mcRun(com._args[1]);
	} else {
		for(auto outside_mc_function : _outside_mc_functions) {
			if(com._name == outside_mc_function._name && 
				((com._args.size()-1)/2) >= outside_mc_function._min_args &&
				((com._args.size()-1)/2) <= outside_mc_function._args_types.size()
				) {
				_content += outside_mc_function.parseSMPLComand(com);

				return;
			}
		}
        throw  "file: " + com._file + " in line: " + com._lines + " unknown operation " + com._name;
    }
}
//--------------------------------------//
string McFunction::mcPrint(SMPLComand com) {
	string out = "";
	out += "tellraw @a [";
    if (com._args[0] != "(" || com._args[com._args.size()-1] != ")") throw  "file: " + com._file + " in line: " + com._lines + " missing '(' or ')'";
	for(int i = 1; i < com._args.size()-1; i++) {
		if(i%2 == 1) {
        string tmp = com._args[i];
		if (tmp[0] == '"'){ // string
			out += "{\"text\":" + tmp + "}"; 
		} else { // int
			out += "{\"score\":{\"name\":\"" + tmp + "\",\"objective\":\"SMPL\"}}";
		} 
        if (i<com._args.size()-2) out += ",";
		} else if(com._args[i] != ",") {
            com.error("missing ','");
        }
	}
	out += "]\n";
	return out;
}
std::string McFunction::mcRun(std::string path) {
	std::string Out = "";
		Out += "function " + path +"\n";
	return Out;
}

