#include "include/McFunction.hpp"
using namespace std;

string McFunction::mcAddInt(string a) {
	string out = "";
	if (this->_base_int_tabs) {
		out += "scoreboard players set " + a + " SMPL 0\n";
	} else {
		this->_base_int_tabs = true;
		out += mcAddIntTab("SMPL");
		out += "scoreboard players set " + a + " SMPL 0\n";
	}
	return out;
}
string McFunction::mcAddIntTab(string a) {
	string out = "";
	this->_tab_int_list.push_back(a);
	out += "scoreboard objectives add " + a + " dummy\n";
	return out;
} 

string McFunction::mcOperationInt(SMPLComand com) {
	string out = "";
	
	if (com._name == "=") {
		out += mcIntSet(com._args[0],com._args[1]);
	 }else if (com._name == "+=") {
		out += mcIntAddEqual(com._args[0],com._args[1]);
	} else if (com._name == "-=") {
		out += mcIntSubtractEqual(com._args[0],com._args[1]);
	} else if (com._name == "*=") {
		out += mcIntMultiplyEqual(com._args[0],com._args[1]);
	} else if (com._name == "/=") {
		out += mcIntDivideEqual(com._args[0],com._args[1]);
	} else if (com._name == "%=") {
		out += mcIntModuloEqual(com._args[0],com._args[1]);
	}

	return out;
} 
string McFunction::mcIntSet(string result, string a) {
	string out = "";
	if (isNumber(a)) { 
        out += "scoreboard players set " + result + " SMPL " + a + "\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL = " + a + " SMPL\n";
	}
	return out;
}
string McFunction::mcIntAddEqual(string result, string a) {
	string out = "";
	if (isNumber(a)) {
		out += "scoreboard players set .$operation_tmp SMPL " + a + "\n";
		out += "scoreboard players operation " + result + " SMPL += .$operation_tmp SMPL\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL += " + a + " SMPL\n";
	}
	return out;
}
string McFunction::mcIntSubtractEqual(string result, string a) {
	string out = "";
	if (isNumber(a)) {
		out += "scoreboard players set .$operation_tmp SMPL " + a + "\n";
		out += "scoreboard players operation " + result + " SMPL -= .$operation_tmp SMPL\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL -= " + a + "SMPL\n";
	}
	return out;
}
string McFunction::mcIntMultiplyEqual(string result, string a) {
	string out = "";
	if (isNumber(a)) {
		out += "scoreboard players set .$operation_tmp SMPL " + a + "\n";
		out += "scoreboard players operation " + result + " SMPL *= .$operation_tmp SMPL\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL *= " + a + " SMPL\n";
	}
	return out;
}
string McFunction::mcIntDivideEqual(string result, string a) {
	string out = "";
	if (isNumber(a)) {
		out += "scoreboard players set .$operation_tmp SMPL " + a + "\n";
		out += "scoreboard players operation " + result + " SMPL /= .$operation_tmp SMPL\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL /= " + a + " SMPL\n";
	}
	return out;
}
string McFunction::mcIntModuloEqual(string result, string a) {
	string out = "";
	if (isNumber(a)) {
		out += "scoreboard players set .$operation_tmp SMPL " + a + "\n";
		out += "scoreboard players operation " + result + " SMPL %= .$operation_tmp SMPL\n";
	} else {
		out += "scoreboard players operation " + result + " SMPL %= " + a + " SMPL\n";
	}
	return out;
}
