#include "include/SMPLComand.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

SMPLComand::SMPLComand(string name, vector<string> args, string lines, string file) {
    _name = name;
    _args = args;
    _lines = lines;
    _file = file;
}

void SMPLComand::print() {
    cout << "_name : " << _name << endl;
    cout << "_args : number of elements " << _args.size() << endl;
    cout << '\t';
    for (auto s : _args) {
        cout << "\"" << s << "\"";
    }
    cout << endl;
    //cout << "_lines : " << _lines << endl;
    //cout << "_file : " << _file << endl;
}

void SMPLComand::error(string error) {
    throw  "file: " + _file + " in line: " + _lines + " " + error;
}