#include "include/McFunction.hpp"
#include "include/SMPL.hpp"
#include <fstream>
#include <filesystem>
#include <iostream>
#include <string>

using namespace std;

SMPL::SMPL() {
	this->_name = "smpl_out";
	this->_out_path = ".";
    this->_state = IDLE;

	this->_mcfunction = McFunction();

}

void SMPL::setName(string name) {
	this->_name = name;
}
void SMPL::setOutPath(string outPath) {
	this->_out_path = outPath;
}
void SMPL::setDebug(bool debug) {
	this->_debug = debug;
	this->_mcfunction.setDebug(_debug);
}
void SMPL::setOutsideFunctions(vector<OutsideMcFunction> outside_mc_functions) {
	this->_outside_mc_functions = outside_mc_functions;
	this->_mcfunction.setOutsideFunctions(outside_mc_functions);
}


void SMPL::parseSMPLComands(vector<SMPLComand> comands) {
	if(_state == IDLE) {
		_state = INIT;
		_function_names++;
        _mcfunction.renameAndNewContent("init");
		filesystem::create_directories(this->_out_path + "/smpl/data/" + this->_name + "/functions");
		ofstream pack;
		pack.open(this->_out_path + "/smpl/pack.mcmeta", ios::out);
			pack << "{\n\"pack\":{\n\"pack_format\": 9,\n\"description\": \"SMPL\"}}";
		pack.close();
    }
    for(int i =  0; i<comands.size(); i++) {
		SMPLComand com = comands[i];
		try {
        if (com._name == "int") {
            _mcfunction.parseSMPLCommand(com);
        } else if (com._name == "function") {
			if (com._args.size() == 2) {
				//cout<<"push:"<<mcfunction.getName()<<endl;
				_functions.push(_mcfunction);
				_mcfunction.renameAndNewContent(com._args[0]);
			} else com.error("epected 2 arg");
		} else if (com._name == "else" ||com._name == "if" || com._name == "unless" || com._name == "while" || com._name == "as" || com._name == "at" || com._name == "positioned") {			
			string function_name = "internal_function_" + to_string(_function_names); _function_names++;
			com._args[com._args.size()-1] = _name + ":" + function_name;
			if (com._name == "while") {
				com._name = "if";
				_mcfunction.parseSMPLCommand(com);
				_functions.push(_mcfunction);
				_mcfunction.renameAndNewContent(function_name);
				com._name = "while";
				_mcfunction.parseSMPLCommand(com);
			} else if(com._name == "if" || com._name == "unless") {
				//find "if" or "unless" have after '}' "else" 
				int open_bracket = 1;
				int close_bracket = 0;
				bool else_found = false;
				for (int j = i+1; j<comands.size(); j++) {
					if(open_bracket == close_bracket) {
						if (comands[j]._name == "else") else_found = true;
						break;
					}
					if(comands[j]._name == "}") close_bracket++;
					if(comands[j]._args.size() > 0 && comands[j]._args[comands[j]._args.size()-1] == "{") open_bracket++;
				}
				if(else_found) {
					com._name += "_with_else";
				}
				//run parse
				_mcfunction.parseSMPLCommand(com);
				_functions.push(_mcfunction);
				_mcfunction.renameAndNewContent(function_name);
			} else if(com._name == "else" && (com._args[0] == "if" || com._args[0] == "unless")) {
				//find "if" or "unless" have after '}' "else" 
				int open_bracket = 1;
				int close_bracket = 0;
				bool else_found = false;
				for (int j = i+1; j<comands.size(); j++) {
					if(open_bracket == close_bracket) {
						if (comands[j]._name == "else") else_found = true;
						break;
					}
					if(comands[j]._name == "}") close_bracket++;
					if(comands[j]._args.size() > 0 && comands[j]._args[comands[j]._args.size()-1] == "{") open_bracket++;
				}
				if(else_found) {
					com._args[0] += "_with_else";
				}
				//run parse
				_mcfunction.parseSMPLCommand(com);
				_functions.push(_mcfunction);
				_mcfunction.renameAndNewContent(function_name);
			} else {
				_mcfunction.parseSMPLCommand(com);
				_functions.push(_mcfunction);
				_mcfunction.renameAndNewContent(function_name);
			}			
		} else if(com._name == "}") {
			if(com._args.size() == 0) {
				if(_functions.size() == 0) throw "to much '}'";
				_mcfunction.saveFunction(_out_path + "/smpl/data/" + _name + "/functions/");
				_mcfunction = _functions.top();
				_functions.pop();
			} else throw "??? - internal error";
		} else if(com._name == "run") {
			if(com._args.size() != 3) com.error("epected 3 arg");
			com._args[1] = _name + ":" + com._args[1];
			_mcfunction.parseSMPLCommand(com);
		} else {
			_mcfunction.parseSMPLCommand(com);
		}
		} catch (string msg) {
			cout << msg << endl;
		} catch (const char* msg) {
			cout << msg << endl;
		} catch (...) {
			cout << "unknown error msg" << endl;
		}
    }

	if(_functions.size() != 0) throw "missing '}'";
	_mcfunction.saveFunction(_out_path + "/smpl/data/" + _name + "/functions/");		
	// _mcfunction.rename("clear");
	// _mcfunction.newContent();
	// _mcfunction.clean();
	// _mcfunction.saveFunction(this->_out_path + "/smpl/data/" + _name + "/functions/");
}

