#include "include/SMPLFileReader.hpp"
#include "include/SMPLComand.hpp"

#include <string>
#include <vector>
#include <iostream>

using namespace std;

vector<SMPLComand> SMPLFileReader::ComandSimplify(SMPLComand comand, int* tmp_varable) {
    vector<SMPLComand> odp;
    if(comand._name == "=" || comand._name == "+=" || comand._name == "-=" || comand._name == "*=" || comand._name == "/=") {
        string name;
        vector<string> args;
        if(comand._args.size() < 2) {
            if (_debug) for(auto s : comand._args) {cout << s << ' ';} cout << endl;
            throw ("file: " + comand._file + " in line: " + comand._lines + " not enough args");
        }
        if(comand._args.size() == 2) {
            odp.push_back(comand);
            return odp;
        }
        
        {   
            //nawiasy
            bool tmp = false;
            int start;
            
            for(int i = 1; i < comand._args.size(); i++ ) {
                if(comand._args[i] == "(") {
                    tmp = true;
                    args.clear();
                    args.push_back("#$" + to_string(*tmp_varable));
                    name = "=";
                    start = i;
                } else if(comand._args[i] == ")" && tmp) {
                    tmp = false;
                    comand._args[start] = "#$" + to_string(*tmp_varable);
                    auto tmp_list = ComandSimplify(SMPLComand(name,args,comand._lines,comand._file), tmp_varable);                   
                    odp.insert(end(odp), begin(tmp_list), end(tmp_list));
                    args.clear();
                    name = string();
                    comand._args.erase(comand._args.begin()+start+1,comand._args.begin()+i+1);
                    i = 0;
                } else {
                    args.push_back(comand._args[i]);
                }
            }

        }
        {
            name = "=";
            args.clear();
            args.push_back("#$" + to_string(*tmp_varable));
            args.push_back("0");
            odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
            int main_varable = *tmp_varable;
            (*tmp_varable)++;

            //pozostałe
            for(int i = 2; i < comand._args.size(); i++ ) {
                if (comand._args[i] == "*" && i+1 < comand._args.size()) {
                    //if(comand._args[i-1].size() > 1) {
                        if(comand._args[i-1][0] != '#' && comand._args[i-1][1] != '&') {
                            name = "=";
                            args.clear();
                            args.push_back("#$" + to_string(*tmp_varable));
                            args.push_back(comand._args[i-1]);
                            odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
                            comand._args[i-1] = "#$" + to_string(*tmp_varable);
                            (*tmp_varable)++;
                        }
                    //}
                    name = "*=";
                    args.clear();
                    args.push_back(comand._args[i-1]);
                    args.push_back(comand._args[i+1]);
                    odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
                    
                    comand._args.erase(comand._args.begin() + i,comand._args.begin() + (i+2));
                    i--;
                } else if (comand._args[i] == "/" && i+1 < comand._args.size()) {
                    //if(comand._args[i-1].size() > 1) {
                        if(comand._args[i-1][0] != '#' && comand._args[i-1][1] != '&') {
                            name = "=";
                            args.clear();
                            args.push_back("#$" + to_string(*tmp_varable));
                            args.push_back(comand._args[i-1]);
                            odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
                            comand._args[i-1] = "#$" + to_string(*tmp_varable);
                            (*tmp_varable)++;
                        }
                    //}
                    name = "/=";
                    args.clear();
                    args.push_back(comand._args[i-1]);
                    args.push_back(comand._args[i+1]);
                    comand._args.erase(comand._args.begin() + i,comand._args.begin() + (i+2));
                    i--;
                } else if((comand._args[i] == "-" || comand._args[i] == "+") && i+1 < comand._args.size()) {
                    i++;
                } else {
                    if (_debug) for(auto s : comand._args) {cout << s << ' ';} cout << endl;
                    throw "file: " + comand._file + " in line: " + comand._lines + " wrong args count";
                }
            }

            name = "+=";
            args.clear();
            args.push_back("#$" + to_string(main_varable));
            args.push_back(comand._args[1]);
            odp.push_back(SMPLComand(name,args,comand._lines,comand._file));

            for(int i = 2; i < comand._args.size(); i++ ) {
                if(comand._args[i] == "+" && i+1 < comand._args.size()) {
                    name = "+=";
                    args.clear();
                    args.push_back("#$" + to_string(main_varable));
                    args.push_back(comand._args[i+1]);
                    odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
                    i++;
                } else if (comand._args[i] == "-" && i+1 < comand._args.size()) {
                    name = "-=";
                    args.clear();
                    args.push_back("#$" + to_string(main_varable));
                    args.push_back(comand._args[i+1]);
                    odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
                    i++;
                } else {
                    if (_debug) for(auto s : comand._args) {cout << s << ' ';} cout << endl;
                    throw "file: " + comand._file + " in line: " + comand._lines + " wrong args count";
                }
            }


            if(comand._args[0] != "#$" + to_string(main_varable)) {
                name = comand._name;
                args.clear();
                args.push_back(comand._args[0]);
                args.push_back("#$" + to_string(main_varable));
                odp.push_back(SMPLComand(name,args,comand._lines,comand._file));
            }
        }
    } else if (comand._name == "load") {
        if (comand._args[0] != "(" || comand._args[comand._args.size()-1] != ")") throw  "file: " + comand._file + " in line: " + comand._lines + " missing '(' or ')'";
        setPath(comand._args[1]);
        return readFile();
    }

    if(odp.empty()) {
        odp.push_back(comand);
    }

    return odp;
}
    
std::vector<SMPLComand> SMPLFileReader::ComandsSimplifier( std::vector<SMPLComand> list_comands) {
    vector<SMPLComand> odp =  vector<SMPLComand>();
    for (auto s : list_comands) {
		int t = 0;
        vector<SMPLComand> list_tmp = ComandSimplify(s,&t);
        odp.insert(end(odp), begin(list_tmp), end(list_tmp));
    }
    return odp;
}
    
    
