#include "include/OutsideMcFunction.hpp"
#include <tuple>
#include <string>
#include <fstream>
#include <filesystem>
#include <iostream>
using namespace std;

Task::Task() {
	this->_id = 0;
    this->_int_value = 0;
    this->_string_value = "";
}
Task::Task(int id, int int_value, std::string string_value) {
	this->_id = id;
    this->_int_value = int_value;
    this->_string_value = string_value;
}

void Task::print() {
    cout << "id:" << _id << ", " << _int_value << ", \"" << _string_value << "\"" << endl;
}

tuple<std::string, int> Task::doTask(SMPLComand com) {
    switch (_id)
    {
    case 0: // text
        return {_string_value, 0};
        break;
    case 1: // arg
        if(com._args.size() < (_int_value*2)+1 ) com.error("not enough args internal task error");
        return {com._args[(_int_value*2)+1], 0};
        break;
    case 2: // if_args
        if((com._args.size()-1)/2 >= _int_value) {
            return {"", 1};
        } else {
            return {"", 2};
        }
        break;

    default:
        throw "unknown task id";
        break;
    }
    return {"", -1};
}
OutsideMcFunction::OutsideMcFunction() {
    this->_name = "";
    this->_min_args = 0;
}

void OutsideMcFunction::print() {
    cout << "name - " << _name << "; min_args - " << _min_args << endl;
    for(auto t : _tasks) {
        t.print();
    }
}


string OutsideMcFunction::parseSMPLComand(SMPLComand com) {
    if((com._args.size()-1)/2 < _min_args) com.error("not enough args");
    //arg type testing VIP
    string odp = "";
    bool skip = false;
    for(auto task : _tasks) {
        if(task._id == 2 || !skip ) { 
            auto task_odp = task.doTask(com);
            if(get<1>(task_odp) == 0 && !skip) {
                odp += get<0>(task_odp);
            } else if(get<1>(task_odp) == 1 ) {
                skip = false;
            } else if(get<1>(task_odp) == 2 ) {
                skip = true;
            }
        }
    }
    return odp + "\n";
}

vector<OutsideMcFunction> OutsideMcFunction::loadOutsideMcFunctions(string path) {
    std::vector <OutsideMcFunction> functions;
	for(int i = path.length()-1; i>=0;i--) {
		if(path[i] == '/') {
			break;
		}else {
			path.erase(i);
		}
	}
	path += "lib";
	
	for (const auto & entry : std::filesystem::directory_iterator(path)) {
		std::string tmp = entry.path().string();
		if( tmp[tmp.length()-1] == 'e' &&
			tmp[tmp.length()-2] == 'f' &&
			tmp[tmp.length()-3] == 'm' &&
			tmp[tmp.length()-4] == 's' &&
			tmp[tmp.length()-5] == '.') {
			functions.push_back(loadOutsideMcFunction(tmp));
		}
	}
    return functions;
}

OutsideMcFunction OutsideMcFunction::loadOutsideMcFunction(std::string path_to_file) {
    OutsideMcFunction odp;
    std::ifstream inputFile;
	inputFile.open(path_to_file);

    if ( inputFile.is_open() ) {
        inputFile >> odp._name;
        inputFile >> odp._min_args;
        int n;
        inputFile >> n;
        if(odp._min_args > n) throw "OtherFunction " + path_to_file + " - min_args > args";
        for (int i = 0; i < n; i++) {
            if(inputFile.eof()) throw "OtherFunction " + path_to_file + " - incorrect number of arg types";
            int tmp;
            inputFile >> tmp;
            odp._args_types.push_back(tmp);
        }
        while (!inputFile.eof()) {
            Task task;
            string task_type;
            inputFile >> task_type;
            if(task_type == "") break;
            if(inputFile.eof()) throw  "OtherFunction " + path_to_file + " - missing content after " + task_type;
            if (task_type == "text") {
                task._id = 0;
                char t0 = ' ';
                char t1 = ' ';
                while (t1 != '"') {
                    inputFile >> t1;
                    if(inputFile.eof()) throw "OtherFunction " + path_to_file + " - missing \" after text";
                }
                inputFile >> std::noskipws;
                while (true) {
                    t0 = t1;
                    inputFile >> t1;
                    if(t0 != '\\' && t1 == '"') break;
                    if(t1 == '\\') {
                    } else if(t0 == '\\' && t1 == '"') {
                        task._string_value.append(1, t1);
                    }else if(t0 == '\\' && t1 == '\\') {
                        task._string_value.append(1, t1);
                        t1 = ' ';
                    } else {
                        task._string_value.append(1, t1);
                    }
                }
                odp._tasks.push_back(task);
                inputFile >> std::skipws;
            } else if (task_type == "arg") {
                task._id = 1;
                inputFile >> task._int_value;
                odp._tasks.push_back(task);
            } else if (task_type == "if_args") {
                task._id = 2;
                inputFile >> task._int_value;
                odp._tasks.push_back(task);
            }
        }
    } else {
		throw "Couldn't open file" + path_to_file;
	}
	inputFile.close();
	return odp;
    
}

