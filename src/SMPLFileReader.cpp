#include "include/SMPLFileReader.hpp"
#include "include/SMPLRawComand.hpp"
#include "include/SMPLComand.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

SMPLFileReader::SMPLFileReader() {
    fillMap();
};

void SMPLFileReader::fillMap() {
    // done
    _base_comands["int"] = 1;
    //_base_comands["double"] = 1;
    //
    _base_comands["load"] = 1;
    _base_comands["if"] = 1;
    _base_comands["else"] = 1;
    _base_comands["unless"] = 1;
    _base_comands["while"] = 1;
    _base_comands["function"] = 1;
    _base_comands["run"] = 1;
    _base_comands["as"] = 1;
    _base_comands["at"] = 1;
    _base_comands["positioned"] = 1;
    _base_comands["}"] = 1;
    // done
    _base_comands["="] = 1;
    _base_comands["+="] = 1;
    _base_comands["-="] = 1;
    _base_comands["*="] = 1;
    _base_comands["/="] = 1;
    // done
    _base_comands["print"] = 1;
}

void SMPLFileReader::addOutsideMcFunctionToMap(vector<OutsideMcFunction> functions) {
    for(auto OutsideMcFunction: functions) {
        _base_comands[OutsideMcFunction._name] = 2;
    }
}

void SMPLFileReader::setPath(std::string path) {
    _path = std::filesystem::canonical(path);
    _loaded_files.push_back(_path);
}

void SMPLFileReader::setDebug(bool debug) {
    _debug = debug;
}
    
    
