#include "include/SMPLRawComand.hpp"
#include <iostream>
#include <string>

using namespace std;

SMPLRawComand::SMPLRawComand() {
    _comand = string();
    _start = 0;
    _stop = 0;
    _file_path = string();;
}

SMPLRawComand::SMPLRawComand(string comand, int start, int stop, string file_path) {
    _comand = comand;
    _start = start;
    _stop = stop;
    _file_path = file_path;
}

void SMPLRawComand::operator=(const SMPLRawComand &SMPL_raw_comand) {
    this->_comand = SMPL_raw_comand._comand;
    this->_start = SMPL_raw_comand._start;
    this->_stop = SMPL_raw_comand._stop;
    this->_file_path = SMPL_raw_comand._file_path;
    
}

void SMPLRawComand::print() {
    cout << "_comand : " << _comand << endl;
    cout << "_start - " << _start << ", _stop - " << _stop << endl;
    cout << "_file_path : " << _file_path << endl;
}
