#include "include/OutsideMcFunction.hpp"
#include "include/SMPLFileReader.hpp"
#include "include/SMPLComand.hpp"
#include "include/SMPL.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char **argv) {

	bool debug = false;
    string out_path = ".";
    string name = "smpl_out";

	if(argc == 1) {
		std::cout << "Nie podano pliku" << std::endl;
		return -1;
	}

	for (int i = 1;i < argc; i+=1) {
		std::string tmp = argv[i];
		if(tmp =="-h" || tmp == "--help") {
			std::cout << "-o --output <path>  set output path" << std::endl;
			std::cout << "-n --name <name>    set main mcfunction name " << std::endl;
			std::cout << "-d --debug          show debug info" << std::endl;
			return 0;
		}else if(tmp =="-o" || tmp == "--output") {
			i+=1;
			out_path =  (string)argv[i];
		}else if(tmp == "-n" || tmp == "--name") {
			i+=1;
			name = (string)argv[i];
		}else if (tmp == "-d" || tmp == "--debug") {
			debug = true;
		}
	}

	std::ifstream inputFile;
	inputFile.open(argv[argc-1]);

	if ( !inputFile.is_open() ) {
		std::cout << "Nie poprawny plik" << std::endl;
		inputFile.close();
		return -1;
	}
	inputFile.close();
	// load outside functions ak mc commands
	


	// run
	try {
		vector<OutsideMcFunction> functions = OutsideMcFunction::loadOutsideMcFunctions(argv[0]);
		SMPLFileReader reader = SMPLFileReader();
		reader.addOutsideMcFunctionToMap(functions);
		reader.setPath(argv[argc-1]);
		reader.setDebug(debug);
		SMPL smpl = SMPL();
		smpl.setName(name);
		smpl.setOutPath(out_path);
		smpl.setOutsideFunctions(functions);
		smpl.setDebug(debug);
		vector<SMPLComand> list = reader.readFile();
		
		if(debug) {
			for (auto l : list) {
				l.print();
			}
		}
		smpl.parseSMPLComands(list);
	} catch (const std::exception& e) {
		cout << e.what() << endl;
	} catch (const std::string &s) {
		cout << s << endl;

	}
	
	
	


	return 0;
} 
